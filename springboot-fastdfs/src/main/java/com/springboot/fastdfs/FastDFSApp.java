package com.springboot.fastdfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;

import com.springboot.fastdfs.comm.config.FastDFSConfig;


/**
 * 
 * @desc 启动类
 * @author chay
 * @version 2.0.0
 * @date 2018-07-12
 */
@SpringBootApplication
public class FastDFSApp {
	
	public static void main(String[] args ) throws Exception{
		// 接收启动时传进来的参数
		FastDFSApp.receiveTrackerParams(args);
        SpringApplication.run(FastDFSApp.class, args);  
        System.out.println( "start success..." );
    }

	/**
	 * 接收启动时传进来的参数
	 * @param args
	 * @throws Exception 
	 */
	public static void receiveTrackerParams(String[] args) throws Exception {
		//文件服务器地址、文件服务器端口	eg. 192.168.1.157 22122
		if(args.length>0){
			FastDFSConfig.CLIENT_TRACKER_ADDRESS = new String[(args.length/2)];
			FastDFSConfig.CLIENT_TRACKER_PORT = new int[(args.length/2)];
			int idx = 0;
			for(int i=0;i<args.length;i++){
				String tAddress = args[i];
				String tPort = args[i+=1];
				FastDFSApp.setTrackerServer(idx,tAddress, tPort);
				System.out.println("join:{TRACKER_ADDRESS:"+tAddress+",TRACKER_PORT:"+tPort+"}");
				idx+=1;
			}
		}else {
			System.out.println( "Client params is null.Default read fdfs_client.conf" );
		}
	}
	/**
	 * 
	 * @param trackerAddress
	 * @param trackerPort
	 * @throws Exception 
	 */
	public static void setTrackerServer(int i,String trackerAddress,String trackerPort) throws Exception{
		if(StringUtils.isEmpty(trackerAddress)||StringUtils.isEmpty(trackerPort)){
			throw new Exception("跟踪服务器的地址和端口都必须填写");
		}
		FastDFSConfig.CLIENT_TRACKER_ADDRESS[i] = trackerAddress;
		FastDFSConfig.CLIENT_TRACKER_PORT[i] = Integer.parseInt(trackerPort);
	}
}
